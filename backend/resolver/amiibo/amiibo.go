package amiibo

import (
	"context"
	"errors"
	"fmt"
	"github.com/golang/protobuf/ptypes/any"
	resolverv1 "github.com/lyft/clutch/backend/api/resolver/v1"
	"github.com/lyft/clutch/backend/resolver"
	"github.com/lyft/clutch/backend/service"
	"github.com/lyft/clutch/backend/service/topology"
	"github.com/uber-go/tally"
	amiibov1 "gitlab.com/khannd1/ilt-clutch/backend/api/amiibo/v1"
	amiiboresolverv1 "gitlab.com/khannd1/ilt-clutch/backend/api/resolver/amiibo/v1"
	amiiboservice "gitlab.com/khannd1/ilt-clutch/backend/service/amiibo"
	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"

	protodeprecated "github.com/golang/protobuf/proto"
	"github.com/lyft/clutch/backend/gateway/meta"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)
var Name="ilt-clutch.resolver.amiibo"
var typeURLName=meta.TypeURL((*amiibov1.Amiibo)(nil))
var typeSchemas = resolver.TypeURLToSchemaMessagesMap{
	typeURLName: {
		(*amiiboresolverv1.AmiiboName)(nil),
	},
}
func New(cfg *any.Any, logger *zap.Logger, scope tally.Scope) (resolver.Resolver, error) {
	logger.Debug("Init resolver")
	var topologyService topology.Service
	if svc, ok := service.Registry[topology.Name]; ok {
		topologyService, ok = svc.(topology.Service)
		if !ok {
			return nil, errors.New("incorrect topology service type")
		}
		logger.Debug("enabling autocomplete api for the lyftcore resolver")
	}

	svc, ok := service.Registry["gateway.service.amiibo"]
	if !ok {
		return nil, errors.New("no amiibo service was registered")
	}

	client, ok := svc.(amiiboservice.Client)

	schemas, err := resolver.InputsToSchemas(typeSchemas)
	if err != nil {
		return nil, err
	}

	r := &res{
		topology: topologyService,
		schemas:  schemas,
		client: client,
		logger: logger,
	}

	return r, nil
}

type res struct {
	topology topology.Service
	schemas  resolver.TypeURLToSchemasMap
	client amiiboservice.Client
	logger *zap.Logger
}

func (r *res) Schemas() resolver.TypeURLToSchemasMap {
	return r.schemas
}

func (r *res) Search(ctx context.Context, typeURL, query string, limit uint32) (*resolver.Results, error) {
	r.logger.Debug(fmt.Sprintf("Search %s- %s - %d",typeURL,query,limit))
	switch typeURL {
	case typeURLName:
		//patternValues, ok, err := meta.ExtractPatternValuesFromString((*amiiboresolverv1.AmiiboName)(nil), query)
		//if err != nil {
		//	return nil, err
		//}
		//
		//if ok {
		//	ctx, handler := resolver.NewFanoutHandler(ctx)

			a,_:= r.client.GetAmiibo(ctx,query)
			//handler.Channel() <- resolver.NewFanoutResult(a,err)
			//defer handler.Done()
			msg:=make([]proto.Message,0)
		for _, v := range a {
			msg=append(msg,v)
		}
		var failures []*status.Status
		return &resolver.Results{
			Messages:        msg,
			PartialFailures:failures,
		}, nil

	}
	return nil,status.Error(codes.Internal,"error")
}

func (r *res) Resolve(ctx context.Context, typeURL string, input protodeprecated.Message, limit uint32) (*resolver.Results, error) {
	return nil,status.Error(codes.Internal,"not implement")

}

func (r *res) Autocomplete(ctx context.Context, typeURL, search string, limit uint64) ([]*resolverv1.AutocompleteResult, error) {
	return nil,status.Error(codes.Internal,"not implement")
}

