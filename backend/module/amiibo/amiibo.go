package amiibo

import (
    "context"
    "encoding/json"
    "errors"
    "fmt"

    "github.com/golang/protobuf/ptypes/any"
    "github.com/uber-go/tally"
    "go.uber.org/zap"

    "github.com/lyft/clutch/backend/module"
    "github.com/lyft/clutch/backend/service"
    amiibov1 "gitlab.com/khannd1/ilt-clutch/backend/api/amiibo/v1"
    amiiboservice "gitlab.com/khannd1/ilt-clutch/backend/service/amiibo"
)

const Name = "gateway.module.amiibo"

func New(params *any.Any, logger *zap.Logger, scope tally.Scope) (module.Module, error) {
    data,err:=json.Marshal(params)
    if err==nil{
        logger.Debug(string(data))
    }
    logger.Debug(fmt.Sprintf("New %s - scope ",Name))
    svc, ok := service.Registry["gateway.service.amiibo"]
    if !ok {
        return nil, errors.New("no amiibo service was registered")
    }

    client, ok := svc.(amiiboservice.Client)
    if !ok {
        return nil, errors.New("amiibo service in registry was the wrong type")
    }

    return &mod{client: client,logger: logger}, nil
}

type mod struct {
    client amiiboservice.Client
    logger *zap.Logger
}

func (m *mod) Register(r module.Registrar) error {
    amiibov1.RegisterAmiiboAPIServer(r.GRPCServer(), m)
    return r.RegisterJSONGateway(amiibov1.RegisterAmiiboAPIHandler)
}

func (m *mod) GetAmiibo(ctx context.Context, request *amiibov1.GetAmiiboRequest) (*amiibov1.GetAmiiboResponse, error) {

    m.logger.Debug("GetAmmnibo " + request.GetName())
    a, err := m.client.GetAmiibo(ctx, request.Name)
    if err != nil {
        return nil, err
    }
    return &amiibov1.GetAmiiboResponse{Amiibo: a}, nil
}