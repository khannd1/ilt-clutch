package main

import (
	"github.com/lyft/clutch/backend/gateway"

	"gitlab.com/khannd1/ilt-clutch/backend/cmd/assets"
	amiibomod "gitlab.com/khannd1/ilt-clutch/backend/module/amiibo"
	"gitlab.com/khannd1/ilt-clutch/backend/module/echo"
	amiiboresolver "gitlab.com/khannd1/ilt-clutch/backend/resolver/amiibo"
	amiiboservice "gitlab.com/khannd1/ilt-clutch/backend/service/amiibo"
)

func main() {
	flags := gateway.ParseFlags()

	components := gateway.CoreComponentFactory

	// Add custom components.
	components.Modules[echo.Name] = echo.New
	components.Resolvers[amiiboresolver.Name]=amiiboresolver.New

	components.Modules[amiibomod.Name] = amiibomod.New
    components.Services[amiiboservice.Name] = amiiboservice.New

	gateway.Run(flags, components, assets.VirtualFS)
}
