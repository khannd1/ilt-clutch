module gitlab.com/khannd1/ilt-clutch/backend

go 1.16

require (
	github.com/bufbuild/buf v0.37.0
	github.com/envoyproxy/protoc-gen-validate v0.6.1
	github.com/fullstorydev/grpcurl v1.8.2
	github.com/golang/protobuf v1.5.2
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.5.0
	github.com/lyft/clutch/backend v0.0.0-20210812214317-190ab75939bd
	github.com/lyft/clutch/tools v0.0.0-20210812214317-190ab75939bd
	github.com/shurcooL/vfsgen v0.0.0-20200824052919-0d455de96546
	github.com/stretchr/testify v1.7.0
	github.com/uber-go/tally v3.4.2+incompatible
	go.uber.org/zap v1.19.0
	google.golang.org/genproto v0.0.0-20210701133433-6b8dcf568a95
	google.golang.org/grpc v1.38.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0
	google.golang.org/protobuf v1.27.1
)
