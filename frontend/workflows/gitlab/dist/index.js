import HelloWorld from "./search";
import Resolver from "./resolver";
const register = () => {
  return {
    developer: {
      name: "duckhan",
      contactUrl: "mailto:khannd@inspirelab.io"
    },
    path: "gitlab",
    group: "Gitlab",
    displayName: "Gitlab",
    routes: {
      landing: {
        path: "/",
        displayName: "Gitlab",
        description: "Gitlab.",
        component: HelloWorld
      },
      resolve: {
        path: "/resolve",
        displayName: "Resolver",
        description: "Gitlab Resolver",
        component: Resolver,
        requiredConfigProps: ["resolverType"]
      }
    }
  };
};
export default register;
//# sourceMappingURL=index.js.map
