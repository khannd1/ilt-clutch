import React from "react";
import { Wizard, WizardStep } from "@clutch-sh/wizard";
import _ from "lodash";
import { Button, ButtonGroup, client, Table, TableRow, TextField, useWizardContext } from "@clutch-sh/core";
import { useDataLayout } from "@clutch-sh/data-layout";
const AmiiboLookup = () => {
  const { onSubmit } = useWizardContext();
  const userInput = useDataLayout("userInput");
  const onChange = (event) => {
    userInput.assign({ name: event.target.value });
  };
  return /* @__PURE__ */ React.createElement(React.Fragment, null, /* @__PURE__ */ React.createElement(TextField, {
    onChange,
    onReturn: onSubmit
  }), /* @__PURE__ */ React.createElement(ButtonGroup, null, /* @__PURE__ */ React.createElement(Button, {
    text: "Search",
    onClick: onSubmit
  })));
};
const AmiiboDetails = () => {
  const amiiboData = useDataLayout("amiiboData");
  let amiiboResults = amiiboData.displayValue();
  if (_.isEmpty(amiiboResults)) {
    amiiboResults = [];
  }
  return /* @__PURE__ */ React.createElement(WizardStep, {
    error: amiiboData.error,
    isLoading: amiiboData.isLoading
  }, /* @__PURE__ */ React.createElement(Table, {
    columns: ["Name", "Image", "Series", "Type"]
  }, amiiboResults.map((amiibo, index) => /* @__PURE__ */ React.createElement(TableRow, {
    key: index
  }, amiibo.name, /* @__PURE__ */ React.createElement("img", {
    src: amiibo.imageUrl,
    height: "75px"
  }), amiibo.amiiboSeries, amiibo.type))));
};
const Amiibo = ({ heading }) => {
  const dataLayout = {
    userInput: {},
    amiiboData: {
      deps: ["userInput"],
      hydrator: (userInput) => {
        return client.post("/v1/amiibo/getAmiibo", {
          name: userInput.name
        }).then((response) => {
          var _a;
          return ((_a = response == null ? void 0 : response.data) == null ? void 0 : _a.amiibo) || [];
        });
      }
    }
  };
  return /* @__PURE__ */ React.createElement(Wizard, {
    dataLayout,
    heading
  }, /* @__PURE__ */ React.createElement(AmiiboLookup, {
    name: "Lookup"
  }), /* @__PURE__ */ React.createElement(AmiiboDetails, {
    name: "Details"
  }));
};
export default Amiibo;
//# sourceMappingURL=search.js.map
