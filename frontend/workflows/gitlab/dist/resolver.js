import { client, Resolver, Table, TableRow, useWizardContext } from "@clutch-sh/core";
import { useDataLayout } from "@clutch-sh/data-layout";
import { Wizard, WizardChild, WizardStep } from "@clutch-sh/wizard";
import React from "react";
import _ from "lodash";
import * as apiPackage from "@ilt-clutch/api";
const InstanceIdentifier = ({ resolverType }) => {
  const { onSubmit } = useWizardContext();
  const resolvedResourceData = useDataLayout("resourceData");
  const inputData = useDataLayout("inputData");
  const onResolve = ({ results, input }) => {
    debugger;
    console.log(results);
    resolvedResourceData.assign(results);
    inputData.assign(input);
    onSubmit();
  };
  return /* @__PURE__ */ React.createElement(Resolver, {
    type: resolverType,
    searchLimit: 10,
    onResolve,
    apiPackage
  });
};
const AmiiboDetails = () => {
  debugger;
  const amiiboData = useDataLayout("resourceData");
  let amiiboResults = amiiboData.displayValue();
  console.log(amiiboData);
  if (_.isEmpty(amiiboResults)) {
    amiiboResults = [];
  }
  return /* @__PURE__ */ React.createElement(WizardStep, {
    error: amiiboData.error,
    isLoading: amiiboData.isLoading
  }, /* @__PURE__ */ React.createElement(Table, {
    columns: ["Name", "Image", "Series", "Type"]
  }, amiiboResults.map((amiibo, index) => /* @__PURE__ */ React.createElement(TableRow, {
    key: index
  }, amiibo.name, /* @__PURE__ */ React.createElement("img", {
    src: amiibo.imageUrl,
    height: "75px"
  }), amiibo.amiiboSeries, amiibo.type))));
};
const Amiibo = ({ heading, resolverType }) => {
  const dataLayout = {
    resourceData: {},
    inputData: {}
  };
  return /* @__PURE__ */ React.createElement(Wizard, {
    dataLayout,
    heading
  }, /* @__PURE__ */ React.createElement(InstanceIdentifier, {
    name: "Lookup",
    resolverType
  }), /* @__PURE__ */ React.createElement(AmiiboDetails, {
    name: "Details"
  }));
};
export default Amiibo;
//# sourceMappingURL=resolver.js.map
