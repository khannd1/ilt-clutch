import { client, Resolver, Table, TableRow, useWizardContext } from "@clutch-sh/core";
import { useDataLayout } from "@clutch-sh/data-layout";
import { Wizard, WizardChild, WizardStep } from "@clutch-sh/wizard";
import React from "react";
import type { ResolverConfigProps, WorkflowProps } from ".";
import _ from "lodash";
import * as apiPackage from "@ilt-clutch/api";
  
export interface ResolverChild extends WizardChild, ResolverConfigProps {}

const InstanceIdentifier: React.FC<ResolverChild>  = ({ resolverType }) => {
    const { onSubmit } = useWizardContext();
    const resolvedResourceData = useDataLayout("resourceData");
    const inputData = useDataLayout("inputData");

    const onResolve = ({ results , input}) => {
        debugger
      // Decide how to process results.
     // Thanks to the defined search limit we know the results will be limited to 1.
        console.log(results);
      resolvedResourceData.assign(results);
      inputData.assign(input);

      onSubmit();
    };
  
    return <Resolver type={resolverType} searchLimit={10} onResolve={onResolve}  apiPackage={apiPackage}/>;
  };


  const AmiiboDetails: React.FC<WizardChild> = () => {
      debugger
    const amiiboData = useDataLayout("resourceData");
    let amiiboResults = amiiboData.displayValue();
    console.log(amiiboData)
    if (_.isEmpty(amiiboResults)) {
      amiiboResults = [];
    }
  
    return (
      <WizardStep error={amiiboData.error} isLoading={amiiboData.isLoading}>
        <Table columns={["Name", "Image", "Series", "Type"]}>
          {amiiboResults.map((amiibo, index: number) => (
            <TableRow key={index}>
              {amiibo.name}
              <img src={amiibo.imageUrl} height="75px"/>
              {amiibo.amiiboSeries}
              {amiibo.type}
            </TableRow>
          ))}
        </Table>
      </WizardStep>
    );
  };


  const Amiibo: React.FC<WorkflowProps> = ({ heading, resolverType }) => {
    const dataLayout = {
     resourceData: {},
     inputData: {},
    //   amiiboData: {
    //     deps: ["resourceData"],
    //     hydrator: (resourceData: { name: string }) => {
    //       return client
    //         .post("/v1/amiibo/getAmiibo", {
    //           name: resourceData.name,
    //         })
    //         .then(response => {
    //           return response?.data?.amiibo || [];
    //         });
    //     },
    //   },
    };
  
    return (
      <Wizard dataLayout={dataLayout} heading={heading}>
      <InstanceIdentifier name="Lookup" resolverType={resolverType} />
        <AmiiboDetails name="Details" />
      </Wizard>
    );
  };
  
  export default Amiibo;