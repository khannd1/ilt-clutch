import type { BaseWorkflowProps, WorkflowConfiguration } from "@clutch-sh/core";

import HelloWorld from "./search";
import Resolver from "./resolver";

export interface ResolverConfigProps {
  resolverType?: string;
}
export interface WorkflowProps extends BaseWorkflowProps,ResolverConfigProps {}

const register = (): WorkflowConfiguration => {
  return {
    developer: {
      name: "duckhan",
      contactUrl: "mailto:khannd@inspirelab.io",
    },
    path: "gitlab",
    group: "Gitlab",
    displayName: "Gitlab",
    routes: {
      landing: {
        path: "/",
        displayName: 'Gitlab',
        description: "Gitlab.",
        component: HelloWorld,
      },
      resolve: {
        path: "/resolve",
        displayName:'Resolver',
        description: "Gitlab Resolver",
        component: Resolver,
        requiredConfigProps: ["resolverType"]
      },
    },
  };
};

export default register;
